---
title: "Contact Me"
date: 2018-01-01T10:59:21+10:00
draft: false
menu: "main"
meta: true
---
I haven't setup comments on this blog. If you've got a comment, question, or just want to get in touch, please use this form.
<form name="contact" method="POST" action="/contact-thanks" netlify>
  <p>
    <label>Your Name: <input type="text" name="name"></label>
  </p>
  <p>
    <label>Your Email: <input type="email" name="email"></label>
  </p>
  <p>
    <label>Message: <textarea name="message"></textarea></label>
  </p>
  <p>
    <button type="submit">Send</button>
  </p>
</form>
