+++
categories = ["tech"]
date = "2018-06-11T08:51:46+10:00"
draft = false
tags = ["ideas", "web"]
title = "Gitbook for Church Documentation"

+++
A few months ago I had an idea of using [Evernote](https://evernote.com) to write and distribute things like _Job Descriptions_ for things which happen around church (see [this post]({{< relref "evernote-is-for-sharing.md" >}})). However, due to Evernote's pricing, I'm looking into other tools which can do the same sorts of tasks.

A tool I'm trying out for publishing church documentation is [Gitbook](https://gitbook.com). It's a service aimed at technical writers—a place to write and publish user manuals. And I think it's a good option for hosting the documents, policies, and procedures for church.

This is something I've seen churches achieve using [wiki software](https://en.wikipedia.org/wiki/Wiki "Wikipedia on Wikis"). That may be a good option. However, it would typically require installing, hosting, and maintaining wiki software on a web server. And most wiki's don't have the most attractive styling.

# Benefits of Gitbook

## It's not the church website.

Although you could store these documents on your church website, it's not the kind of thing potential visitors need to know. It's also not something you necessarily want coming up when people search for your church on Google.

## It's not your Church Management System

I think ChMS's (like [Elvanto](https://elvanto.com) or [Jethro](https://easyjethro.com.au)) are essential for doing things around church. And most ChMS have a facility for storing church documentation. However, in my situation, not many people in the congregation are confident around computers, and so our ChMS is completely 'behind the scenes'. Other than automated roster emails, very few people know anything about the system we use. So something with a much lower barrier-to-entry, which doesn't require yet another account and password, makes sense to me.

## It's free (to a point)

Ok, it's not the most generous freemium product around. For no cost you can only create [two spaces](https://www.gitbook.com/pricing), one public and one private (a _space_ is their terminology for a book/collection of documents), and you can only have two people contribute to the project (though more could help if you connect to [github](https://github.com). However, I think these limitations won't be an issue for most churches who are wanting a place to write and distribute their policies and procedures.

## It uses git

Ok, this is a nerdy one, and it can be completely invisible to you if you want, but behind the excellent user-interface is all the power of [git](https://en.wikipedia.org/wiki/Git) version control. You can set up branches for when you're writing new versions of policies while keeping the old version intact. You can roll back to a previous version, or see a _diff_ of changes which have been made.

## It's 'Live'

This is something I wrote about earlier [with Evernote]({{< relref "evernote-is-for-sharing.md" >}}).

You could write your church's documentation in a word processor and then send around PDF's of the book, possibly even attaching the booklet to every email reminder. But regularly attaching a file to emails feels a bit _heavy_. Sending around files also could run into problems from circulating different versions of documents. Using Gitbook (or Evernote) avoids this problem as the current version lives online.

A benefit of Gitbook over Evernote is that it's a _book_ (or an online equivalent of one). Evernote has a quick way to share a single note as a webpage; you can't do the same with notebooks (you can only share them with other Evernote users). Gitbook also automatically creates a table of contents for both the whole book and each _page_. Finally, I prefer Gitbook's editor. It gives you access to simple styles (heading levels, bold, italic), which Evernote lacks.

# What have I done

Over the last few weeks, I've been writing drafts of 'Job Descriptions' for roles like welcoming, morning tea, reading the Bible at church. I'm now sending links to these documents to some key leaders for them to review. I've put a link to the Gitbook space down the bottom of our regular roster reminder email. In the future, as someone takes on a new job, I'll either email them a link to a page in the Gitbook or print it out for them.

For privacy, I've made the space _public_ but _unlisted_. This is a reasonable level of privacy as making something _private_ on the Internet come sometimes give you the false sense that no unintended eyes will ever see the information. So by being _unlisted_, the job descriptions should end up in search engine results, but anyone who I give the link to can read without needing a Gitbook account.
