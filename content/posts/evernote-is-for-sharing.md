+++
date = "2018-04-14T14:16:18+10:00"
tags = ["evernote", "ideas", 'iOS']
title = "Evernote is for sharing"
categories = ["tech"]
+++
Recently our family went on a holiday. When we there I had an idea—our kids should keep a little journal of the things we've done. Not only would it be a cute memory, but it would be a fun way for them to keep working on literacy and creatively learning without realising it! Now, of course, I didn't come up with this idea until the holiday had started, so I didn't plan ahead and think about a nice journaling notebook. But I did have my iPad, and scraps of paper. So I what I did was, each afternoon or morning, I let one child type their journal into [Evernote](https://evernote.com/), and the other wrote/drew something on a scrap of paper. I then photographed the handwritten note and inserted it into the note. And each day we’d add something to the note—photos, or typed and hand-written notes.

\(This was inspired by a discussion I heard last week on [Mac Power Users](https://www.relay.fm/mpu/424) where Mike Schmidtz spoke about children using tech for _creating_ and not only _consuming_)

Now, that's not a unique idea. I'm sure many people keep a journal in Evernote. However, to add value to the journal, I then shared the _public link_ out of Evernote and sent it to the grandparents/aunties and uncles who I knew would appreciate hearing what the children were writing. And each day they could check the note via the link to see if there’d been any updates. This got me thinking about ways the _Public Link_ could be used in churches.

Now, there's lots of great ways that we can share documents online—[Dropbox](https://dropbox.com), [Google Drive](https://drive.google.com), or the many free and simple website/blog creators. However, a free website is very public, it'll be catalogued by search engines, and most of the cloud file-sharing services and online word processors require people to create an account, even just to read the link. Or they are a way to share files, which are then downloaded and not kept up-to-date. The benefits of a publicly shared note are that it's accessible to anyone who can click on a link without an Evernote account, it's only available to people who have the link (which, is pretty insecure, but it's something), and it's instantly updated. It also can be worked on collaboratively—though you need a paid account for this, and all editors need an Evernote account.

So how could a shared Evernote document be used in ministry? Maybe you could

* write a _job description_ for volunteer roles,
* have a note with information about this week's church services (although a [ChMS](https://en.m.wikipedia.org/wiki/Church_software#Church_management_software) would be a better fit for this job - but if you aren't using one...),
* keep a list of songs you sing, with information about key signatures, where the music can be found, links to Spotify/YouTube recordings,
* details about a church event - like timetables, packing list, transport arrangements...

Some of these things you could include on your church website, though it's not really relevant for people looking into your church for the first time. It could also be emailed around, or discussed on a Facebook group - though then you'd get lost in threads within threads. It's definitely the kind of thing church management systems are good at - but they can be overwhelming for less techy members, and even for _digital natives_ they often need training.

Now there are limitations to this. Currently, you can't embed videos into notes (though you could link to a video on Dropbox/YouTube/Google Drive/Vimeo), and I did come across some synchronisation issues which needed to be manually fixed. There’s also no notifications sent when things are updated. But for many use-cases, it should do the job.

{{< tweet user="ds_says" id="984999554771705857" >}}

So next time you need to share a 'living document' give Evernote a shot.

(If you’d like full instructions on sharing public links, [I found some very detailed instructions here](http://www.harmonenterprises.com/blog/evernote-public-links-for-data-sharing).)
