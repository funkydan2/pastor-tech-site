+++
categories = ["tech"]
date = 2021-09-03T05:31:13Z
tags = ["MacOS", "keynote"]
title = "Move Animation in Apple Keynote"

+++
When preaching, I often display maps on the screen. Maps are great, but the problem is giving people both context and detail. And so, what I've often done with keynote is created two slides - one with the 'zoomed out' map image and the next 'zoomed in' on the relevant location.

However, by jumping between two images, it can be hard for people to remember where the detail comes from on the map.

This is where the _Move_ animation in Apple Keynote (which I've just discovered, after years of trying all sorts of other sliding type transitions) comes to the for. The 'move' animation is not to be confused with _Magic Move_. _Magic Move_ is for animation _objects_ on a slide (for example, an arrow or box that moves around to highlight different things). The _Move_ works with images and enables you to scroll and/or zoom around an image.

I couldn't find any videos showing how to do this with a recent version of Keynote - and so to demonstrate, I made this quick screen recording.

{{<youtube UU7wfPZDLGw>}}

(The video below uses an earlier version of Keynote, but was helpful in working things out.)

{{<youtube 9qzRravWfV0>}}