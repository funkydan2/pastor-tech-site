---
title: "Why Podcast Sermons (Part 1)"
date: 2018-04-03T16:40:42+10:00
tags: ["howto", "web"]
draft: false
---
(This post is part 1 [of a series]({{< relref "sermon-podcast-with-wordpress-contents.md" >}}) on recording and distributing sermons online.)

Lots of churches record their sermons. It probably goes back to the days of cassette records, where a dedicated volunteer (or team) would spend hours checking on a tape duplicator, getting tapes ready either for congregation members or the mail. These days, most churches that record sermons, distribute their sermons via the internet (though, last week, I heard about a church that was still using tapes).

But *why* do you want to record sermons? For some, it's because their church or preacher desires to have an international platform. They want to be the next John Piper or Tim Keller, or the next Joel Osteen or Brian Houston.

![Screenshot of iTunes podcast ‘Religion and Spirituality’ charts](/uploads/2018/04/SermonPodcastHowto/itunesPodcast.png)

Now, if God chooses to use your preaching in that way (hopefully of the former), then that's tremendous, but I'm not sure that [making a name for yourself](http://ref.ly/Gen11:4) is something which brings honour to Christ!

So if you're not aiming to be famous, why would you put sermons up for the world to hear? I reckon there are a few good reasons—as part of shepherding the flock in the church, and being a way potential new-comers can 'taste and see' what you're on about.

# Serving the flock
We all know not everyone who's part of your church can be there every Sunday. For some, it's because being *regular* means being part of the gathering one a month. However, there are other reasons people can't gather with God's people as they would like to—they may be sick or caring for someone who is sick, they may be a shift-worker, or be travelling. Then some people are *'at church'* but not *at church* such as people working in Children's ministry. These people are still sheep who need to be fed, and one of the ways we can feed them with God's Word is to make the preaching of God's word available to them.

Having sermons available online can also be used to evangelise, or to encourage people in other churches. I occasionally have people from church mention they have shared a sermon from our church with non-believing family or friends, or to build up a Christian friend who they think would appreciate the sermon. Now although I don't preach for those aren't there, and I don't record for them, it's been encouraging to see this unintended benefit.

# Serving new-comers
For many people, before they visit a church, they'll check the church out online. Having sermons available online means they can have a *taste* of what church is like before coming along. Now, church is more than a sermon, but it's a way to get a feel of the service that might be more accurate than the stock-photos, or the aspirational vision statement found on the website. Even if you decide to have a featured sermon for newcomers to listen to—the best of your best—it's still real, live, you!
