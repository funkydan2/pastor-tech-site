+++
categories = ["tech"]
date = "2019-09-30T08:18:11+10:00"
tags = ["mac", "windows", "MacOS"]
title = "Automatically Login to Windows 10 or MacOS"
+++
In many churches I've been involved with, the computer used for projecting song lyrics has no need to be secured by a password. This is because the computer doesn't have any sensitive information on it—just song lyrics and slides. Since a number of people use these computers, signing in with a password is an inconvenience, and often the password ends up written on a sticky-note somewhere anyway!

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/jopoe/6378526057/in/photolist-aHDBzc" title="Defeats the purpose, no?"><img src="https://live.staticflickr.com/6042/6378526057_bdefe76620_z.jpg" width="478" height="640" alt="Defeats the purpose, no?"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

At [my church](https://gympiepresbyterian.org.au "Gympie Presbyterian Church"), the data projector laptop is running Windows 10 (MacOS doesn't work well with the wireless projection system). For months I've been frustrated by the login system (which seems to change with every other update). But now I've finally found [instructions on how to automatically log in to Windows 10](https://www.intowindows.com/how-to-automatically-login-in-windows-10/ "Into Windows"). Method 1 on this page worked for me.

If you're running MacOS, logging in without a password [is much simpler](https://support.apple.com/en-us/HT201476 "Apple KB Article")
