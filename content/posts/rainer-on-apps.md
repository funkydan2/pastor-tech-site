+++
date = "2018-04-12T17:41:36+10:00"
tags = ["podcast", "apps", "review"]
title = "Rainer on Apps"
categories = ["tech"]
+++
The next series of posts I'm wanting to do is [Take 5](/tags/take-5). In this series I'll share brief introductions to five iOS, Mac, and Web apps/services which I've found useful for ministry.

But before I get to that, I just heard the latest episode from Rainer on Leadership. In the episode Thom and Jonathan give [15 (or was it 16) apps for church leaders](https://thomrainer.com/2018/04/favorite-apps-church-leaders-use-rainer-leadership-422). This is a podcast I listen to regularly, and I need to give it a full review sometime soon. However, since this episode was on topic for this blog, I thought I should share it here.

Of the apps they mention, there are a few which don't (yet) work in Australia (*Amazon Prime*, *Doctor on Demand*, and *Shipt*). I'm also not familiar with the *Three Circle* evangelistic presentation, so can't recommend it (though I'm sure it's worth a look, especially as the [Two Ways to Live](http://twowaystolive.com) app hasn't been updated to work with the latest version of iOS. However, most of the other apps are good options, and most are free, [so give the episode a listen!](https://thomrainer.com/2018/04/favorite-apps-church-leaders-use-rainer-leadership-422/)

*Edit: I've now written my first 'Take 5' and it's on [iOS Apps]({{< relref "take-5-ios.md" >}})*
