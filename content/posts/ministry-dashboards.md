---
title: "Ministry Dashboards"
date: 2018-07-25T13:36:57+10:00
tags: ["data", "disciple making"]
draft: false
---
Over the last month or so I've been hearing lots about *Ministry Dashboards*. Basically, *Ministry Dashboards* are about how a church measures and tracks the health of their church.

{{< figure src="/uploads/2018/07/chris-leipelt-254045-unsplash.jpg" attr="Photo by Chris Leipelt on Unsplash" attrlink="https://unsplash.com/photos/4UgUpo3YdKk?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText" >}}

The post which kicked things off was over at [Pro Church Tools](https://prochurchtools.com), Brady Shearer recommended looking into all kinds of [*next steps* churches should be tracking](https://blog.nucleus.church/church-growth/) (you might want to jump down to the section titled "17 'next steps'"). It's a thought-provoking list, which includes metrics as diverse as *giving* and *social media engagement*.

However, must more helpful are two audio resources from [Geneva Push](//genevapush.com). First up is a short (~15 min) podcast episode on [Ministry Dashboards](https://genevapush.com/theonething/episode-14-ministry-dashboards/). It's a good introduction, but worth following up with the [longer recording from their conference](https://genevapush.com/resources/knowing-what-to-measure-and-why/) (the link is broken, so you'll need to [download it through this direct link](https://s3-ap-southeast-2.amazonaws.com/genevaaudio/knowing-what-to-measure-and-why.mp3).

Because of my tech/hacking approach to things, this has got me thinking. I know I'm tracking a lot of this data, though am I using this information to see God's kingdom growing? To use [Tim Clemens words](https://genevapush.com/resources/knowing-what-to-measure-and-why/), I'm good at focusing on 'making pies,' but how am I doing at running a pie shop? (Though I'm not sure if calling the church a *disciple making machine* is the best analogy.)
