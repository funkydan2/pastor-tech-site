+++
categories = ["time"]
date = "2018-06-07T22:15:13+10:00"
draft = false
tags = ["email"]
title = "How I learnt to love my inefficient email workflow"

+++
I have an inefficient workflow for writing emails.

Now, this might seem odd. Surely writing an email is a simple as

1. open email program/website
2. click 'new email'
3. write
4. click send.

And for many emails, this is the case.

However, when I'm writing a serious message, such as a mass email to the church or a formal email, I process what I've written through [Grammarly](https://grammarly.com). Grammarly doesn't have a plugin for the email app I use ([Airmail](http://airmailapp.com)) and it also doesn't work with the web interface for my email provider. And so, the workflow for writing these more critical emails is:

1. Compose a first draft of the email in a markdown editor. I use [Typora](https://typora.io) on Mac and [Drafts](https://agiletortoise.com/drafts) on iOS.
2. (Mac) Copy the markdown (plain-text) from Typora and paste it into the Grammarly app.(iOS) Switch to the Grammarly keyboard.
3. Within Grammarly, consider their corrections and proofread.
4. (Mac) Copy the corrected and proof-read markdown formatted text out of Grammarly and paste into the markdown pane of Airmail. (iOS) Use the Drafts [action](https://actions.getdrafts.com/a/1DH) to send the markdown formatted text to Airmail.
5. Proof-read again.
6. Repeat steps 2-5 until confident.
7. Press send.

Now, this is a convoluted process. I've often wished either Airmail or my email's web interface integrated with Grammarly. But just today I've realised this convoluted process is a great way to prevent emails being sent in the heat of the moment.

I've heard people suggest a way to wisely deal with angry emails is to write the email [but leave the 'to' box empty](https://www.entrepreneur.com/article/231734), then go for a walk before you press send. It's great advice. I've also found it's a good idea to wait before you press send on mass emails/email campaigns because I often remember that one more thing I needed to say _just after I press send_.

<iframe src="https://giphy.com/embed/yAAJyaWeuRgAg" width="480" height="301" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/reaction-yAAJyaWeuRgAg">via GIPHY</a></p>

What I realised today is my convoluted Grammarly work-flow, which I was getting quite impatient with, has this very wise practice _built-in_. Yes, it means these emails take longer to write, and often don't get sent in with the speed of a fiery email thread. But, it gives me time to think about my words. So I've come to love this little inefficiency.
