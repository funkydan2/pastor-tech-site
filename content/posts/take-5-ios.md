---
title: iOS Take 5
date: 2018-04-25 08:34:34 +1000
tags:
- review
- iOS
- take5
categories: ["tech"]

---
[Take 5](/tags/take5) is going to be a series of 5 short reviews of tools/podcasts/hacks I find useful in ministry.

{{< youtube PHdU5sHigYQ >}}

I'm going to kick off the series with five [iOS](/tags/ios) apps I use all the time. They are:

1. [Drafts]({{< relref "#drafts" >}})
2. [N-Track Tuner]({{< relref "#n-track-tuner" >}})
3. [Authy]({{< relref "#authy" >}})
4. [VLC]({{< relref "#vlc" >}})
5. [Beanhunter]({{< relref "#beanhunter" >}})

# Drafts

At first, [Drafts](https://agiletortoise.com/drafts/) seems to be just another note taking app, and with the recent improvements to Apple Notes, you might wonder why it exists.

The way I used Drafts is as an _inbox_ for text. When someone asks 'do you have a pen and paper,' if my iPhone or iPad is nearby I'll go straight for Drafts. When people grab me before or after church, it's where I jot down todos or email addresses. The reason for using Drafts for this is

1. by default the app opens to a blank note, ready to capture text and
2. it's easy to get your notes _out_ of Drafts and into where it needs to go (email, task manager, file in iCloud or Dropbox).

I also use Drafts when I'm entering text into web-based forms because it's all too common to lose a paragraph or two of work when something goes wrong.

{{< figure src="/uploads/2018/04/drafts.jpeg" alt="Drafts Screenshot" caption="I started this blog post in Drafts." >}}

[Drafts 5 has just been released.](https://agiletortoise.com/drafts/) The previous version cost a few dollars just to get started, but the new version is working on the free/pro (subscription) model. The subscription version allows you to make _actions_ and has a variety of themes, but I think most people will find the free version very functional.

# N-Track Tuner

Even if you're not a musician yourself, music is a part of church as we ['let the Word of Christ dwell in us richly'](http://bib.ly/Col3:16). It helps when the ['joyful noise'](https://bib.ly/Ps98:4) is vaguely in tune. This is a great app so when a muso asks 'does anyone have a tuner' (which they shouldn't, but invariably do) you can quickly say 'yes' and put your iPhone or iPad in front of them.

![N-Track Tuner Screenshot](/uploads/2018/04/n-track-tuner.jpeg)

[N-Track Tuner is free in the appstore](https://itunes.apple.com/us/app/n-track-tuner/id409786458?mt=8). It's ad-supported, and I've used it so much, that I happily paid for to remove the ads.

# Authy

This isn't specifically ministry related, but with the number of logins, we now use for different things, and the growing awareness of privacy and security, [you really should be using two-factor-authentication](https://www.makeuseof.com/tag/what-is-two-factor-authentication-and-why-you-should-use-it/).

There are a few 2FA apps out there (Google makes a well known one), and Authy is my app of choice because it integrates seamlessly with their Wordpress plugin.

![Authy Screenshot](/uploads/2018/04/authy.png)

[Authy is free in the appstore](https://itunes.apple.com/us/app/authy/id494168017?mt=8).

# VLC

VLC is a well-known media player on Mac and PC. It's been around for many years now. Although the interface is ugly (to put it nicely) it's popular because it is rock solid (it can play almost any video or audio you throw at it) and it's reasonably lightweight, running well on even low powered machines.

On iOS, the interface is attractive, and it continues to be a reliable way to play video and audio. It has an interesting way of getting files into the app. Along with download from the standard cloud file sharing services, you can _upload_ into VLC through its embedded web server. I find the easiest way to get files into the app is to temporarily turn this on, visit the website using Safari on my Mac  (VLC tells you the IP address you need to use), and then drag and drop videos from the Mac finder into the web app.

![VLC Screenshot](/uploads/2018/04/vlc.jpeg)

VLC is a useful tool for ministry. I store videos for Bible studies and training courses in the app (I probably should also find a few good evangelistic videos to keep there too). If I'm in a group setting, I can plug my iPad into a TV or projector, or if I'm meeting it's someone in a cafe or my office, we can both look onto the iPad screen.

[VLC is available for free](https://www.videolan.org/vlc/download-ios.html).

# Beanhunter

The way to find (and rate) the best coffee. Essential if you're meeting someone in a different town or suburb!

[Get it now](https://www.beanhunter.com/mobile)!
