---
title: "Reading Plan: Serving without Sinking"
date: 2023-02-06T08:47:44+10:00
tags: ["book"]
---
Recently, I re-read John Hindlay's little book [*Serving Without Sinking*](https://www.thegoodbook.com/serving-without-sinking). This time I was discussing it with a bloke from church. It's a great book to diagnose some ways our hearts deceive us as we serve God from false motives and paints a beautiful picture of the God who serves us.

One thing lacking from the book are discussion questions. I'm not always a fan of discussion/reflection questions in books—but for reading with someone they're a very useful. So as I was reading, I wrote my own questions. I'm not claiming they're the best, but they're a start. (Please [get in touch]{{< ref "contact.md" >}}) if you've got any suggestions for improving the discussion questions.)

I've also included a reading plan. The chapters in the book are quite short, so it's possible to discuss a few at a time. The chapters also are 'chunked' into similar topics so the discussion should work better if you read the right chapters together (I didn't do this...but will next time).

# Reading Plan

1. Chapters 1-4 (The problem)
2. Chapters 5-8 (Who Jesus makes us to be)
3. Chapters 9-11 (Loved to love, served to serve)
4. Chapters 12-13 (Final reflection: Joyful service)

# Discussion Starters

## Chapter 1 - Introduction

- Which of the four people sounded like the 'now' you?
- Which of the five relationships to serving summarises your experience?

## Chapter 2 - Serving can be joyful

- Read Matthew 11:28-30. Honestly, how do you respond to this invitation and promise?
 - How do you 'half believe' this verse?
- 'Working for this Boss can be, and should be, restful, joyful, wonderful.' Do you feel this is true? What would it take to be true?

## Chapter 3 - A wrong view of God

Do you resonate with any of these *wrong* views of Jesus?

- Serving Jesus to be good enough for him (E.g. praying pharisee and tax collector)
- Serving Jesus to get something from him (e.g. ' older brother' from the prodigal son)
 - Do you resonate with any of the ways this expresses itself? Looking for 'church growth'? Thinking God will be against you because you've failed? Bitter because you've done your best but God hasn't come through?
- Serving Jesus to pay him back
 - Gratitude from love vs gratitude from being indebted

## Chapter 4 - A wrong view of people

- Do you serve out of one of these *wrong* views of others?
 - To impress others. To be thanked/rewarded.
 - 'Would I do this thing if I knew that no one, other than me and God, would ever know I'd done it?'
 - To belong, to be welcomed by a group.
- Do you serve out of one of these *wrong* views of yourself?
 - Because Jesus needs me. Because I need to matter.
 - Do you ask yourself the 'what if' questions? What if I stopped serving, 'would Jesus still achieve his purposes'?
 - Without depending on Jesus/without prayer.
- Do you resonate with the story of Jim at the end of the chapter? Do you need to 'give up' serving so you don't 'give up' on Jesus?

## Chapter 5 - Served by Christ

- How does it hit you that Christians are not servants but are served by Jesus?
 - How does this truth fit with the language of being slaves of Christ?

## Chapter 6 - Friends with the Boss

- According to John 15:13-15, what's the difference between being a servant and a friend?
- How might you explain how you serve/give at church in terms of Jesus being your friend (not only your boss)?

## Chapter 7 - Bride of the King

- How has this chapter changed how you think about the way the relationship between Christ and the Church is a marriage?
- Do you know Jesus' love for you?
- Do you love Jesus?
- How does loving Jesus lead to serving him?

## Chapter 8 - Sons of the Father

- What does it mean to be an adopted son of God?
- What difference does being an adopted son make for serving God?
- What is the treasure of Christianity? Do you feel this?

## Chapter 9 - Still being served

- 'We are, above all, people who are served, not people who serve.' How does Jesus still serve you?
- How is it an encouragement to know Jesus is praying for you?
- How is the staff and rod (prodding) of Jesus a way of him serving you?

## Chapter 10 - The gift of serving

- How is serving God a gift from him?
- How have you benefited from other people using their gifts to serve God?
- When have you experienced closeness to God through serving him?
- How has God gifted you to serve? How does using these gifts help you to lean on others, depend on Jesus, make you more like Jesus, and enable you to show Christ to others?

## Chapter 11 - Serving is love

- When have you found yourself *wanting* to serve God? Can you think of a time you've experienced blessing or felt joy at serving God?
- Why do you obey God?
- What is the risk of serving without love? What is the outcome of serving from love?

## Chapter 12 - Slaves with a Master

- How was slavery in the ancient world different from what we might think?
- Jesus says we can’t serve two masters, implying we will serve one. How Is ‘freedom’ actually slavery?
- ‘The character of the master we serve defines the life that we live.’ How does this make serving Jesus true freedom?

> We are not free not to be a servant. So when we don’t serve Jesus as tall, we’re still serving—we’re serving a different ‘god’. And when we serve but with the wrong motives, we’re serving a false god too—worshipping a god who says we must earn his help, or worshipping an idol of popularity, or busy-ness, and so on.

## Chapter 13 - The joy of serving Jesus

- Having read this book, what will you do differently?
- How do you (do you?) pray about the areas in which you serve?
- In what ways is serving making it hard for you to love Jesus?
- Is there an area where you should stop serving? How could you make this happen?
- To which of the ‘two sons’ do you most relate? Why? How does setting God as the loving, embracing Father impact how you approach serving him?
