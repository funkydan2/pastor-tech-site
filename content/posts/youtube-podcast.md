+++
title = "Get your Podcast on YouTube"
date = 2024-02-19T14:03:55+10:00
images = ["/uploads/2024/rss2yt.png"]
tags = ["Web"]
categories = ["tech"]
+++

Listening to [Mac Power Users](https://www.relay.fm/mpu/732) I heard about a new(ish) feature where YouTube will import podcasts for listening to on YouTube.

We've had a podcast hosted on the church website and listed with Apple Podcasts, Spotify, and the other major Podcast libraries for years--but lots of people at church or looking for a new church aren't Podcast listeners. But they do know how to YouTube.

To host your Podcast on YouTube, all you need is an RSS feed (if you've got a podcast, then you've got this). But there are a few catches
1. Your podcast must not contain any copyright music. Should be fine for a sermon podcast, unless you've got intro/outro music.
2. If you have 'sponsor' messages, this needs to be declared. Once again, probably not in a sermon podcast.

To get started:
1. Go to the YouTube menu (top right corner) and click 'YouTube Studio)
    ![YouTube Menu](/uploads/2024/ytmenu.png)
2. In studio, click `Content` from the left side menu, then click the `Podcasts` tab.
    ![YouTube Studio Podcasts Tab](/uploads/2024/ytpodcast.png)
3. In the Podcasts tag click `Create New` and follow

Once YouTube has imported your podcasts, by default they're listed as *Private*. You'll need to manually change the visibility to *Public* if you want people to be able to find your podcast. (This can be done in bulk, but takes a few minutes to get right.)
