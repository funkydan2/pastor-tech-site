+++
title = "Twitter Timeline"
date = 2022-05-19T12:32:34+10:00
lastmod = 2022-05-20T08:11:20+10:00
images = []
tags = ["web","ideas"]
categories = ["tech","time"]
+++
This week I've made a small change to Twitter that's been good for my *soul* and *productivity*.

A bad habit I've developed over many years is taking regular, 'short' breaks to look at what's happening on social media (I'm sure I'm the only one who does this). Recently my drug of choice has been Twitter—maybe because of the short, hot takes it serves up, I've been able to convince myself _I'll only do this for a minute or two_. But, of course, this isn't true. The system is designed to keep you scrolling and clicking, seeing more content, giving more data, and viewing more ads.

What's the change I've made? It's changing the Twitter timeline from _top Tweets_ to _latest Tweets_; that is, Twitter now displays tweets from people I follow (and ads) in chronological order, rather than exposing what the software deems to be 'top' *based on* who I follow.

How has this helped?

## Soul
What's the drug that Twitter prescribes? Outrage, shock, sadness, and (occasionally) joy.

What Twitter thought was *top* to me was tweet-storms about the latest outrageous thing Christians or politicians had said and everyone's 'hot takes' on the news of the day. Maybe for someone, somewhere, this is important. They need to know what 'they' are saying and how to respond. But all it was doing was making me angry, sad, and frustrated at things that aren't (in any meaningful way) part of my world—very few of the people in my world (church, community, family) are interested or impacted by these events.

Changing to the *lastest Tweets* timeline means I now hear from the people I choose to follow about things that matter to them...and thus, more likely me. I do want to know things that are happening in my friend's lives or in the Christian community in Australia (and the world). I will pray for these things, and some knowledge of broader trends is helpful for navigating life in Christ at this time. But I don't need the spiral of outrage from the *influencers* of 'Christian' Twitter (weird or otherwise).

Getting rid of the noise is good for my soul. Hearing all the *hot takes* of the latest offense just makes me angry, anxious, and sad. There are enough things to be concerned or upset about in the lives of people I care for than to also carry the burdens of the world.

## Productivity
And this has led to a boost in productivity. To be honest, the chronological tweets of those I follow are not as addictively interesting as the outrage storm.

Too often I have gone down rabbit holes, just trying to work out what the outrage is about. Why all the vague tweets about leggings or the 'third way' (to name a few 'highlights' of the last few weeks)? Who said what? How did this start?

Sadly, I think there's something appealing about feeling outraged or disgusted. Maybe it's not unlike the Pharisee's prayer, 'thank you, that I'm not like ... sinners'. Feeling outraged, or seeing the 'worst' of what someone has said makes me feel better about myself! And that kind of cheap affirmation is addictive.

Moving the 'most recent' setting doesn't quite have the same drawcard. There are lots of fairly normal, prosaic tweets. I don't get drawn down as many rabbit holes. It doesn't fire up my rage. And so I actually get bored after a minute or two...and so I get back to doing what I should.

# How do I do this?
It's easy to make the change, and once you do it appears to 'stick'...at least for the browser/device you're accessing Twitter on.

To change how the timeline, click the 'starry' button right at the top right of your timeline (see below).

![Image of the starry change timeline icon](/uploads/2022/05/change-timeline-icon.png)

Once you click on this image, there's one option, which is to change from *top Tweets* to *latest Tweets*.

![Image of the change timeline menu](/uploads/2022/05/change-timeline-menu.png)

And if you want to change back, follow the same process.

There's probably not much to be done to make Twitter good, but at least this might make it less bad and more manageable.
