---
title: "Record Sermon (Part 4)"
date: 2018-04-05T20:32:08+10:00
tags: ["howto", "audacity"]
draft: false
---
This tutorial is going to explain how to record using the free program, [Audacity](http://www.audacityteam.org). There are other ways you could record a sermon, such using a dedicated recorder or even your phone. However, Audacity is a good option as it's free and runs well even on older computers.

# 1. Plug things in
To get started, you'll need to connect your computer to your church's PA system. Most older PA systems will have an [RCA line output](https://en.wikipedia.org/wiki/RCA_connector), whereas newer ones may have a USB output.

If you are recording an analogue signal you'll need a cable that has two *RCA* connectors at one end and a *headphone (3.5mm)* connector at the other.
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/mac_users_guide/3653407848/in/photolist-6yQESu-7oEhFo-bBKwDh-bBKwzw-bD92Z2-bBKxv3-bf76nP-8db7z1-bQEccF-bQEcMB-c5EkPh-bBKxcs-bBKxd9-odPBkK-8s2SW-Bb9o-bBKubf-bBKwLA-22BVY7-7NtQdy-71SDTk-d8NF9s-bBKuPb-bQEcL2-bBKuw9-4Ruewa-5LmpQK-7mJtWm-5LrCGL-bBKuJ5-ajnxfB-7JwdML-5LmDrB-71WFA5-5LmpQP-4BpviG-bQEbtH-ajnx8Z-xzVL3-7QgndY-9bq9Nk-7Qgn1Q-Gc6AeF-7QfZoL-ajnxik-dtouU-6fKMrD-8tZhw8-bgBd-mEiUx7" title="RCA to 1/8&quot; Stereo Cable"><img src="https://farm3.staticflickr.com/2448/3653407848_b3092bfd27_z.jpg?zz&#x3D;1" width="640" height="427" alt="RCA to 1/8&quot; Stereo Cable"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Plug the 3.5mm plug into the input of your computer. The input will usually have a picture of a microphone or an arrow going *into* a circle (or it's not the one with the headphones picture). Not all newer computers have a microphone/line input. So you may need to purchase a USB sound card which has an audio input.

![Audio Inputs](/uploads/2018/04/SermonPodcastHowto/AudioInput.jpg)

# 2. Set up Audacity for recording
There are a few settings you need to check in Audacity.

* Audio input selection. This is the drop-down list beside the microphone icon. You'll need to set the input to something like *line-in* or *microphone*. If your computer has a built-in microphone or a webcam, there may be a few options in this list.

![Mono/Stero](/uploads/2018/04/SermonPodcastHowto/Audacity-Mono.png)

![Sample Rate](/uploads/2018/04/SermonPodcastHowto/Audacity-SampleRate.png#float-right)

* Recording settings. Since we're only recording speech, you can save some disk space by lowering the quality of your recording. I record the sermon at 22050Hz in mono. The sample rate is set down the botom of the screen and the mono/stereo setting is changed by clicking the drop-down list right next to where the audio input is selected.

* Audio input volume. For this, you need to balance the volume of the signal coming from the mixing desk and the volume of the computers' audio input. The only way to get this right is to try a few options. You can change the input volume by moving the slider next to the microphone icon in Audacity. The aim is to have the blue *waveform* (it's a visual representation of the audio) to not ever hit the 1.0 line, but to sit around 0.5.

![Volume ](/uploads/2018/04/SermonPodcastHowto/Audacity-Volume.png)

![Waveform](/uploads/2018/04/SermonPodcastHowto/Audacity-Waveform.png)

Once you have a reasonable level, you're all set to record. It's simply a matter of pressing record sometime before the sermon starts and pressing stop when it finishes.
I like to do a quick test record before the service, and save it with the file name I want. Then I delete the test recording (just click the little x button for the track) then save again (yes, Audacity will warn you that you're saving a blank project). The reason I do this is I can click ⌘-S or Ctrl-S when the recording finishes, and I know everything is saved correctly.

# 3. Edit recording
There are a few edits which are quick and easy to do in Audacity.

The first is to remove the extra audio from the start and end of the sermon. To do this,

1. listen to your recording until you hear the place you want to start,
2. click where you want to start. You can move this *cursor* using the arrow keys to make small adjustments to the cursor location. You can test whether you've got the right moment by using your spacebar to play and pause.
3. Select from the start of the audio up until the cursor: *Edit->Select->Track Start to Cursor*.
4. Press *delete*.

![Selected Audio](/uploads/2018/04/SermonPodcastHowto/Audacity-Selection.png)

The process is almost the same to remove the end of the recording, except you use the *Edit->Select->Cursor to Track End* menu item.

You can also remove pauses and silences in your recording. Truncate silences is an easy way to make the recording a bit shorter for listeners, and the file slightly smaller. To do this,

1. Select your whole recording (⌘-A or *Edit->Select->All*).
2. Open the *Truncate Silence* effect—*Effects->Truncate Silence*.
3. At first try these settings: Level -30db and Duration 1.5 seconds. (You may find others which work better for you.).

![Truncate Silence](/uploads/2018/04/SermonPodcastHowto/Audacity-Truncate.png#float-right)

Have a listen to your recording, and once you're happy, export it as a WAV file (*File->Export Audio*). When you export the audio, Audacity gives you an option to save some *Metadata* such as the artist name and title. You can fill out these details, but it won't be used later.

![Export Audio](/uploads/2018/04/SermonPodcastHowto/Audacity-Export.png)

Now you're ready for the [next step]({{< relref "create-mp3.md">}})—using [Auphonic](https://auphonic.com) to create an MP3 file.
