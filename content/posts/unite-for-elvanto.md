+++
categories = ["tech"]
date = "2019-05-04T10:40:14.000+10:00"
tags = ["church managment system", "web"]
title = "Unite for Elvanto"

+++
On [this week's episode of Mac Power Users](https://www.relay.fm/mpu/480), the guest, Michael Hyatt, mentioned [Fluid](https://fluidapp.com) as a way of separating work 'web apps' from your normal browser.

Fluid creates what's called a [site-specific browser](https://en.wikipedia.org/wiki/Site-specific_browser). As the name suggests, it's web browser dedicated for use on a specific, single site. Why would you want to do this? It's not to solve a _technical_ problem, but a _behavioural_ one. When using a multi-purpose browser, websites can get visually lost in a sea of similar looking tabs or browser windows, but with an SSB websites live in their own window, with an icon in the ⌘-Tab switcher. Another reason is that having work web sites sitting beside Amazon or Twitter too often leads to distraction.

We use Elvanto as our _church management system_. Like every other modern CMS I'm aware of, Elvanto runs _in the cloud_. Since Elvanto is always open in one (or two, or three) tabs in my browser, I thought it was the perfect candidate for a site-specific browser.

In my testing, I tried out both [Fluid](https://fluidapp.com) and [Nativeifier](https://github.com/jiahaog/nativefier), as they were both often mentioned on forums and the like. Fluid is straight forward to use--it's installed as a standard Mac app (download, drag, and drop into the Applications folder) whereas Nativeifier is a command-line application installed through [npm](https://www.npmjs.com/).

Both Fluid and Nativeifier appeared to work great with Elvanto. They created an app named _Elvanto_ which I could drop into the _Applications_ folder on my Mac, and when I ran it, it opened up the login window for Elvanto. I logged in, and almost everything worked as expected. Everything, that is, except for creating a printable version of a service run sheet, a task I do at least once a week. This was a deal-breaker for me. However, after scouring the web, I stumbled across [Unite](https://www.bzgapps.com/unite) which works pretty well with Elvanto's service run sheet printing system.
![Unite Screenshot](/uploads/2019/05/Unite.png)
[Unite](https://www.bzgapps.com/unite) comes with a 14-day free trial, which allows you to create three applications. Once the trial period is over, the full version costs USD $9.99 or is [available with a Setapp subscription](https://setapp.com/get/unite) .
