+++
title = "How to Download Video from a Private Facebook Group"
date = 2023-07-14T15:50:16+10:00
images = []
tags = ["howto"]
categories = ["tech"]
+++
[My church](https://gympiepresbyterian.org.au) livestreams services to a private/closed Facebook group. We started this a couple of years before COVID-19 hit to care for a family who'd moved to a remote town where the closest church was more than an hour away. That family has now moved back to us, but we continue to livestream for sick and frail members.

Our livestream setup is deliberately very crude—it's an old Android phone on a tripod, streaming over a 4G mobile connection.

Recently we had a visiting preacher who wanted a copy of the video for his own records, which lead to solving the problem of downloading livestreamed videos from a private/closed Facebook group.

In the past, this could be done by browsing to the *mobile* Facebook site (swapping the `www` for an `m` in the URL) and then, using Firefox, the video could be downloaded by right-clicking and selecting `Save As...`. But, for a couple of years this hasn't worked. More recently, the way I downloaded the videos required scouring the page source code for references to the video file, and trying to download them--but that took a long time and wasn't all that reliable.

This is my method (works July 2023).

1. Install a video downloading add-on. The one that worked for me was [Video Downloader Professional](https://addons.mozilla.org/en-US/firefox/addon/video-downloader-profession/) for Firefox (they also have a Chrome extension). I can't vouch for the security of this plugin—so I installed it in my not-everyday browser (i.e. not the one I use for banking!). If you don't use multiple browsers, I'd recommend disabling or uninstalling the add-on when not in use.
2. Go to the private facebook group and start playing the video. It probably works best to open the video full-screen, so that it's the only thing the Video Downloading add-on detects.
3. *(This is the trick.)* Click on the Video Downloader Professional add-on and download **both** video files that have a non-zero filesize (see picture below). This is the trick. The larger file has video but no audio. The smaller file has audio but no video.
    ![The pop-up screen from Video Downloader Professional](/uploads/2023/07/downloadFBvideo.png)
4. Import both files into a video editor (I used iMovie). I found that the audio file took a long time to import. Make sure that both the video and audio start at the same time (I found that audio was longer...but the end of silence)
5. Export the video.

It's not that hard—the two tricks are
1. Finding an add-on *that works*.
2. Realising that both 'video' files are needed—otherwise it's just a mime!
