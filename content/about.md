---
title: "About"
date: 2018-01-02T21:42:01+10:00
draft: false
menu: "main"
meta: true
---
G'day.

I'm planning to make this an occasional blog where I share things about using technology in ministry. I'll be writing about Mac, iOS and Web based tools (because that's what I use).

It's a work in progress.

![Logo](/uploads/2019/11/PastorTech_Logo.png)

![License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png#float)

All of my words on this site are licensed under the [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). Images may be copyright by their respective owners. Please check before use.
