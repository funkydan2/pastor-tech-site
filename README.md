A [hugo](https://gohugo.io) built website.

http://pastortech.netlify.com/

Hosted using [bitbucket](https://bitbucket.com) and [netlify](https://netlify.com). Managed using [forestry.io](https://forestry.io).

A blog for things ministry and technology.
