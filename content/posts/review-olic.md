---
title: "Book Review: Organising Love in Church"
date: 2018-04-04T20:17:04+10:00
tags: ["review", "book"]
draft: false
---
[Stuart Heath and Tim Adeney, *Organising Love in the Church*](https://www.gospelgroundwork.com/all/olic).
(Currently $3.99 in Australia in the Kindle and Apple iBooks store).

![Organising Love in Church-Cover](/uploads/2018/04/OLIC_cover.jpg)

About five years ago I read a book that had me thinking 'yes, this is what we believe. Everything in this is obvious. So why don't we do what this book says.' That book was [*Total Church*](https://www.crossway.org/books/total-church-tpb/). It's a book that reimagines church, so it's not as focussed on *events* but *gospel communities* which are localised small groups of Christians 'doing life' for the sake of mission and discipleship.

This week, in preparation for a sermon on [John 13:34-35](https://ref.ly/John13:34-35) I read *Organising Love in the Church*. It was a book which had been on my [Goodreads](https://www.goodreads.com) *To Read* list for a while, and with the inspiration of preaching on *love one-another* I finally got to it. And although it has a different approach and is much shorter, I reckon it lands in the same field as *Total Church*.

When I read *Total Church* I recall feeling inspired, but also somewhat lost, where do we start? It seems we have so many sound structures where the Bible is opened and heard, the Gospel is at the centre, and we spend time together, so why don't we have the mission-oriented community we say we believe in and desire? Here is a strength of *Organising Love in Church*. After setting up the vision of a loving church, which is both evangelising non-believers and growing believers, the final chapter leaves us with some concrete steps to put in place, things a small group (or small church!) could aim to do *this month* to begin the process of cultural change. I'd tell you what they are...but you need to read the book yourself.

# Some quotes & reflections
> When we’re interested in social architecture, we’re really focused on small and large relational networks, not small and large events.

This is central to the thesis of the book. Even in churches which are self-consciously not *program driven* and aim to have light calendars, we are still *event* rather than *network* focussed. In my circles (which are like the authors) this is often blamed on [Knox-Robinson ecclesiology](http://matthiasmedia.com/briefing/2011/12/knoxrobinson-for-today-extended/). Knox and Robinsons' thinking emphasised church (*ecclesia*) as gathering. The (local) church is the gathering or assembling of the people of God around the word of God. Thus the church only exists when gathered. Critics then see there's little space in this thinking for loving relationships like those talked about in this book. However, I think that comes down to misunderstanding the distinction made between *church* and the *people of God*. There's plenty more to think about here.

![Quote](/uploads/2018/04/Organising-Love.jpg)

> 'Love' is the great summary of the Christian life. In his grace — his unmerited kindness — God has loved us; he now calls us to love him and to love the things that he loves.

This becomes an important description of *love*. Love is integral to the gospel which saves and changes believers. And if we love God, then we love the things he loves - other believers, growing in godliness, seeing people saved.

> If our church has orthodox teaching but unchanged lives, we communicate two contradictory messages: on the one hand, we say that following Jesus is the most important thing in the world; on the other, we show that following Jesus makes little or no difference.

This is one of the many wake-up calls in the book. Too often we say the right thing, preach the right thing, but there's no difference. Thankfully, in this book they do more than point out the surface problem, an underlying issue (social architecture) is addressed, and we're given concrete actions for change. Of course, the other issue may be unbelief and unrepentance, but the book isn't addressing that question but rather the things we do which work against the grain of the gospel changing lives.

> If you always ask the question (even implicitly), “What’s good for my club and my culture?”, in the end you’ll be unable to welcome even your own children.

Nuff said.

# For more thinking
I appreciate this book and have been challenged by the authors' vision and suggestions. Having read it myself, I'm going to find a way to get it into the hands of my elders to read together. It puts its finger on significant issues, raises important questions, and, best of all, gives a concrete way forward.

And having read the book, here are some things I need to think more about (and would be helpful to see in a follow-up work):

1. More thinking on ecclesiology. This is linked to what I was saying above about *Knox-Robinson Ecclesiology*. This book worked from a definition of the good news of the Christian life as being 'love'. There are lots of good theological reasons to support this way of thinking, but it would be interesting to explore how the discussion about the *marks* and the *mission* of the church, interact with this model of church as a network of relationships.
2. The authors admit their thinking developed in the context of city ministry. They are 'Sydney Evangelicals'. It also seems they may have been working in larger church contexts (which in Australia probably means 200+ people). However, most churches in Australia are smaller than 100 people, and many would even be within the size of their proposed 'gospel community'—15-40 people. In this very common situation, the church won't be a 'network of gospel communities' but is the gospel community! This situation doesn't negate many of the practical steps to organising love, such as practising hospitality, creating circumstances where Christian and non-Christians can build friendships, and speaking gospel encouragement to each other, but it does nuance their suggestions. Further thinking could also be done in how organising love differs in regional and rural communities or established churches with many older, long-term members.
