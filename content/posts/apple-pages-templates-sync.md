+++
title = "Pages templates now syncs through iCloud"
date = 2019-11-07T07:22:55+10:00
tags = ["pages", "MacOS", "iOS"]
categories = ["tech"]
+++
I use Apple Pages as my main word processing tool. It's what I use for writing sermons, letters, reports, agenda's...any document where I'm the only person writing (for team-authoring, it's [Google Docs](//docs.google.com) or [Zoho Writer](//writer.zoho.com) all the way).

Just this week I discovered that the template chooser now synchronises between the macOS, iOS, and iPadOS versions of Apple Pages.

[![Apple Pages MacOS Template Chooser](/uploads/2019/11/PagesMacTemplateChooser.png)](/uploads/2019/11/PagesMacTemplateChooser.png)
[![Apple Pages iOS Template Chooser](/uploads/2019/11/PagesIOSTemplateChooser.png)](/uploads/2019/11/PagesIOSTemplateChooser.png)

This isn't so significant for the stock provided templates, however for personal templates, it's a another step towards making the shift between devices more smooth. Previously I've kept my templates in the template chooser on my Mac, and also in various folders on iCloud Drive (mainly in the _Pages_ application folder), but this synchronised solution is much neater, and continues to make my iPad a more serious computing option.

I'm don't know when this feature was included. There are [online discussions about it going back a few years](https://discussions.apple.com/thread/8214949), and it's now working on Pages version 5.2 (iOS) and version 8.2 (macOS).

I've [previously posted the templates I use for sermons]({{< relref apple-pages-presenter-mode.md >}}), and I'll link to them again here:

* [Template for printed sermons](/uploads/2018/05/Sermon_Print.template)
* [Template for teleprompter sermons](/uploads/2018/05/Sermon_Teleprompt.template)
