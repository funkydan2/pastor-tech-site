---
title: "Redeeming Time"
date: 2021-02-16T14:41:10+10:00
category: ["time"]
tags: ["theology"]
---
Each of us has the same amount of time each day - 86 400 seconds. But how do you make the most out of every one of those?

For many of us, we think productivity is getting more stuff done in the same amount of time. Fortunately, some wise people have recognised it's about doing the right things. But how does this fit with the gospel - the big story of what God has done, is doing, and will do, in Christ?

Carl Matthei, who's involved in uni ministry in Sydney, Australia, has written two fantastic articles exploring what Paul means in Ephesians 5 when he refers to 'redeeming the time'. You can read them here: [What does it mean to Redeem the Time](https://www.australianchurchrecord.net/what-does-it-mean-to-redeem-the-time/), [Time Management Advice from Ephesians 5](https://www.australianchurchrecord.net/time-management-advice-from-ephesians-5/).
