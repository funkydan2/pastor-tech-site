+++
categories = ["tech"]
date = "2018-06-04T09:54:41+10:00"
draft = false
tags = ["take5"]
title = "Take 5: Free Photo Resources"
+++
Pastoring involves communicating. If you're a pastor, you're engaged in preaching God's word regularly—whether that's through monologues or preparing small group discussions. For many of us, as we communicate we realise the old 'a picture is worth 1000 words' has more than a pinch of truth.

There are many ways images are a great help in pastoring. Whether you're making slides for a Sunday sermon, bringing some colour to your church website, or sharing an image on social media, there are lots of reasons you'll be looking for pictures.

However, pastors often fall afoul of copyright. Many wrongly think 'if it's online, it's free'. And occasionally you hear of churches and pastors who've been caught stealing (and breaking copywriter is a type of theft).

So if you want to use images in your ministry, and you're pushed for time and money, here's five (and one bonus) great ways to legally and ethically find high-quality photos.

## 1. Freely Photos

[Freely Photos](https://freelyphotos.com) is my _go to_ place when looking for images, mainly if I'm looking for something 'Christian', like an image of a Church building, or people praying, or a Bible.

![](/uploads/2018/06/freelyphotos.png)

Every photo on the site is free to use in whatever way you want. Because they're licensed under a [Creative Commons Zero](http://creativecommons.org/publicdomain/zero/1.0/) the images can be used without asking permission or crediting the original artist (though I'm sure a link back to [the site](https://freelyphotos.com) is appreciated).

The site is aimed at Christian and church use, so it has lots of photos of church settings, activities, crosses, and bibles. In addition, it has lots of _general_ images. I suppose you could say it's a Christian version of Pexels or Unsplash [(see below)]({{< relref "#3-4-pexels-and-unsplash" >}}).

Freely Photos has [recently had a crowd-funded rebuild](https://www.kickstarter.com/projects/729491027/freelyphotos-website-re-build). So it's now even easier to find the photo you want.

## 2. Lightstock

Unlike Freely Photos, [Lightstock](https://lightstock.com) is primarily a site which sells stock photos (at a very fair price). However, they release a [free photo](https://www.lightstock.com/free-photo) and [free vector drawing](https://www.lightstock.com/free-vector) every week and a [free stock video](https://www.lightstock.com/free-video) every month!

Lightstock is restricted to Christian artists (they require all contributors to affirm the [Apostle's Creed](https://www.lightstock.com/partner_faqs#becoming-a-partner5)), so have loads of great resources for churches.

Since the free resources are only available for a limited time, I recommend signing up to their mailing list (they usually send out an email about the free photo and vector each Tuesday) so you can add the resources to your own library (I use [Pixave](http://www.littlehj.com/mac/) for this).

## 3 & 4. Pexels and Unsplash

[Pexels](https://www.pexels.com/) and [Unsplash](https://unsplash.com) are both excellent sources of free to use images. Like [Freely Photos]({{< relref "#1-freely-photos" >}}) all photos on the site are available under a [Creative Commons Zero](http://creativecommons.org/publicdomain/zero/1.0/) license. Both have a diverse library of images available, though not as many 'Christian' specific images as Freely Photos and Lightstock.

![](/uploads/2018/06/pexels.png)

I found recently that Pexels has more often come up with an image that works for me. Maybe they've got a better search algorithm, or perhaps they've just got more images in their library. Both are worth a search.

## 5. Flickr 'Creative Commons'

This used to be my number one place to go for images, but I'm now finding more and more useful photos on other sites. However, if I can't see what I'm looking for on Freely, Pexels, or Unsplash, I'll head over to Flickr.

With Flickr, many of the images are not able to be used because of copyright. However, many photographers also license their photos using a [Creative Commons](https://creativecommons.org) license, which allows them to be used _as long as you credit the artist_.

My process for finding Creative Commons images on Flickr is

1. Visit [Flickr](https://flickr.com) and search for the term you're looking for. ![](/uploads/2018/06/flickr_search.png)
2. Filter the search results to see **All Creative Commons**. ![](/uploads/2018/06/flickr_CC.png)
3. Make sure when you use an image, you follow the terms of the license.![](/uploads/2018/06/ScreenShot2018-06-04_at_9.42.33_am.png)

If I'm using an image in a Keynote slide, I generally have a small text box with the (1) title of the image (linked to the sources), (2) name of the artist, (3) details of the license (linked to the license details). I make this text box pretty small, so it doesn't distract people in the congregation, but I make sure it's there!

## Bonus: Take your own!

What do you do if you can't find the image you want (for free). Well, you could buy a photo from [Lightstock](https://lightstock.com) - they're prices are very reasonable. But sometimes there's just not the image you need—so take your own.

Once I wanted to illustrate repentance with a picture of a _U-Turn Permitted_ sign. These are pretty common in Australia at intersections, so I was surprised to not find a fitting image online. So, I went out to the intersection a little way from our home and took the photo myself!

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/lausdeo/14130441997/in/photolist-nwEd7i-nwDMPJ" title="U Turn Permitted Sign"><img src="https://farm4.staticflickr.com/3768/14130441997_1c78820886.jpg" width="375" height="500" alt="U Turn Permitted Sign"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

It's available on Flickr, under a [Creative Commons license](https://creativecommons.org/licenses/by-nc-sa/2.0/), so you're free to use it, as long as you give credit and you're not making any money from it!
