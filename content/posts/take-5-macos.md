+++
title = "macOS Take 5"
date = 2019-05-03T16:00:50+10:00
tags = ["take5", "review", "mac"]
categories = ["tech"]
+++
A quick review of 5 Mac Apps I use, mainly which help deal with text.

1. [aText]({{< relref "#atext" >}})
2. [Copy'em Paste]({{< relref "#copy-em-paste" >}})
3. [PopClip]({{< relref "#popclip" >}})
4. [Accordance Services]({{< relref "#accordance-services" >}})
5. [Bartender]({{< relref "#bartender" >}})

# aText
In ministry, there's lots of writing: emails, letters, sermons, orders of service, blog posts, social media updates. Often there are words or phrases  written over and over again. This is where [aText](http://www.trankynam.com/atext/) comes into play. aText is a menu bar app which turns snippets of text into longer words or phrases to make writing more efficient.

aText comes with a bunch of snippets built in, such as `ddate` expands to today's date (such as 3 April 2019) and `udate` expands to today's date *in universal format* (e.g. 20190403). As a user to can add snippets to its database. For example, I've made two snippets `ssun` and `usun` which expands to the upcoming Sunday's date in different formats. I've also made snippets for contact details and commonly used acronyms. These small snippets save a few keystrokes each time. However, I think the greatest power of applications like aText is for longer bits of text. So I've made a snippet which populates OmniOutliner with the questions and headings I use when planning a sermon, or example ways of introducing the offering during a service. I could also see the benefit of having snippets for creeds and prayers.

aText can also make snippets with variables. I don't use any of these at the moment, but I used to have a snippet for sending our roster reminder emails. The way this worked was I made an email template within aText which had spots for volunteer names. Then, once a week I'd create a new email, type *;roster* in the body, the dialogue box would pop up where I would type in the volunteers for that week, and a fully written email, with all the important information, would be created. (I now have this fully automated using Elvanto, which saves even needing to write the email.)
![aText Screenshot](/uploads/2019/05/aText.png)
There are a few applications in this space. The *gold standard* would have to be Text Expander. It runs as a subscription, at [USD $3.33 a month](https://textexpander.com/pricing/). The only benefit of TextExpander over aText is a corresponding iOS app. I'd love to have the power of aText on my iOS devices, but for me, it's not worth the extra cash.

[aText](http://www.trankynam.com/atext/) comes with a free 21 day trial, and costs USD $4.99.

# Copy'em Paste
Is a *clipboard manager*. Have you ever gone to paste some text or an image into a document, but when you click `⌘-v` what you wanted to paste is gone. Or do you sometimes need to copy a few different things from a webpage, maybe a couple of quotes and the address, which means you've got to switch back-and-forth between applications? A clipboard manager solves these kinds of problems by keeping a history of things which you've copied to the clipboard and providing a way to search through them to find what you're looking for.

There are loads of clipboard managers out there, but I found [Copy'em Paste](https://apprywhere.com/copy-em-paste.html) a few years ago and have settled on it. By default, it stores the last 50 items (which seems to be about right—if something was copied any longer than that, I normally wouldn't think about it). To access your clipping history, you hit `⌘-ctrl-shift-v` and up pops a little window where you can click or search for the item you're looking for. The app also makes it simple to paste the item *exactly* as it was copied (with all the formatting) or as plain-text.
![Copy'em Paste ScreenShot](/uploads/2019/05/CopyEmPaste.png)

[Copy`em Paste](https://itunes.apple.com/au/app/fun-math-games/id876540291?mt=12) is sold in the Mac App Store for AUD $14.99. You also need to install a *free* [helper application](https://apprywhere.com/copyem-paste-helper.html) for it to work.

# PopClip
On iOS, whenever you highlight/select text, a fantastic little menu pops up, offering a bunch of relevant options to dealing with the text—like copying, formatting, or defining in a dictionary. [PopClip](https://pilotmoon.com/popclip/) is an application which replicates this functionality on your Mac.

PopClip has an extensive [directory of plugins](https://pilotmoon.com/popclip/extensions/). I use the word count, [unclutter](https://unclutterapp.com), and copy as markdown plugins. I've also written a plugin for [Accordance](http://www.accordancebible.com), which puts some of the advanced *Copy As* functions to appear in PopClip. I submitted the plugin to the directory, but it's not yet  approved. However [you can download it here](/uploads/2019/05/Accordance.popclipextz). {Update October 2021: changes in version 2021.10 of PopClip broke the preavious Accordance Plugin. You can get a [fixed version here](/uploads/2021/10/Accordance.popclipextz)}

![PopClip Plugins](/uploads/2019/05/PopClip_Plugins.png)
![PopClip Plugins](/uploads/2019/05/PopClip_Accordance.png)

You can get a [free trial of PopClip from the developer's website](https://pilotmoon.com/popclip/), and it sells for [AUD $14.99 on the Mac App Store](https://itunes.apple.com/au/app/popclip/id445189367?mt=12).

# Accordance Services
Accordance is my recommended Bible study software. It's fast and powerful, and did I say _fast_. If you're an Accordance user, you'll want to install the [Accordance Services](https://www.accordancebible.com/Accordance-Installers/#Services).

The Services are accessed through the _contextual menu_ (aka right-click or ⌘-click). There are a few different tools available, but the two I've found useful allow you to insert Bible verses without copying and pasting from Accordance. The are two versions of the Service, one which inserts verses from your default translation, and another that allows you to select a translation.

![Accordance Services Menu](/uploads/2019/05/AccordanceServices.png)

A recent upgrade of the Apple iWork suite (Pages, Numbers, and Keynote) means the Services no longer work with these applications. However, there's an [alternative AppleScript solution](https://www.accordancebible.com/forums/topic/25853-services-broken-macos-10144-accordance-1233/?p=127874) which gets close to replicating the functionality through the Scripts menu.

You can read more about [Accordance Services on the Accordance blog](https://www.accordancebible.com/Accordance-Services).

# Bartender
[Bartender](https://www.macbartender.com) isn't a specific _ministry_ tool, but since the previous four apps run in the menu bar, you'll be wanting something to keep that part of your screen under control. Bartender is a utility which can hide menu bar apps, and only display them when you need them (either when you click the bartender icon or when the application is active).

Bartender comes with a four-week trial and is AUD $22.14 to purchase. ([Dozer](https://dozermac.com/) is a free, open source alternative—but I've not used it so can't recommend.)
