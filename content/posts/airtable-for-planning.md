+++
title = "Airtable for Planning Teaching Programs"
date = 2020-11-20T17:08:52+10:00
lastmod = 2021-03-15T07:11:48+10:00
images = []
tags = ["Web"]
categories = ["tech"]
+++
What tools do you use to plan out your teaching and preaching?

One of the tools I've used the past (and I'm sure I'm not alone) is a spreadsheet—maybe Excel or google sheets. And this works well...for a while. I had a row for each Sunday, planned out preaching into the future. One of the features of most spreadsheets is you can have other sheets within the same file, and in those tabs, I recorded books of the Bible or topics which had been preached at church. A spreadsheet is also quite flexible—with the (seemingly) infinite table size, you can leave notes to your future self about ideas and plans.

However, after a few years (or, honestly, even after one) the spreadsheet can become unwieldy. You've got to scroll for and scroll to find this week's entry. Or if you reverse the order, putting the most current sermon in the top row, you're constantly having to add more rows for new sermons. Then there's rows and columns you've hidden to try and make things look more simple, but then you forgot the information's even there to find.

[Airtable](//airtable.com) is a tool for getting around this problem, and I use it as a key part of my workflow for planning teaching and preaching. (You can have a [look at a blank example base here](https://airtable.com/shrKwLBCTMhApCMM9).)

![Week by Week Table](/uploads/2020/11/airtable-week.png)

Airtable is a web-based tool that takes the 'spreadsheet as a database' concept to the next level. It's not quite a database (at least, it doesn't replicate all the features of a relational database like MS Access) but it's more than a spreadsheet. At first glance, it looks a lot like a spreadsheet. When you create a new base, you're presented with a blank table. But unlike a spreadsheet, the information in this table can be presented in several different *views*, and you can display information from other tables by way of *relations*.

# Views
As the name suggests, views are a way of presenting the information in a base in different ways. Airtable provides loads of really fancy views, like Kanban boards and calendars, but for a teaching planning database, I find the most helpful views are the simple table view, filtered and sorted in ways that reveal the information I want.

So, in my planning base, I have the main view (which shows absolutely everything - just like a google sheet). A view which shows the teaching program for this year and next year, and two views for the next six weeks, one with the information I need particularly for preaching, and another one more useful for service leaders.

![List of views](/uploads/2020/11/airtable-views.png)

These views make finding the information I need pretty easy. I mainly find myself using the 'next six weeks' view, so I know what's happening. At other times I'll want to look at the whole year, so I can think about holidays or rosters.

# Relations
In my teaching base, I have one simple use for relations. There are two tables, one which lists the details for each Sunday, and another which has details about preaching series. In the 'week by week' table, there is a column which links to the 'series' table. In a spreadsheet, I would simply type the name of the series in this column - and that would probably be sufficient. However, with Airtable, the link between these two tables means that I can click on the name of the series in the 'week by week' table, and instantly be taken to the record for the whole series. Another nice feature is that in the 'series' table, I have a column which records the length of a series - and that's automatically calculated. I use this during the planning phase to think about how much time we're spending in different parts of the Bible.

![Series Table](/uploads/2020/11/airtable-series.png)

# Other nice features
* Auto filling dates. Lots of spreadsheets have this feature - but if you put one Sunday's date in the first cell, and then in the next cell down you enter the next Sunday's date, then you can easily drag down the column and Airtable will automatically put the correct date for each Sunday. (And this works ok with Easter and Christmas as well. You just stop dragging when you get to, for example, Good Friday, enter the dates for Easter, then once you've got two Sunday's dates in a row, you can return to auto-filling dates.)
* Web-based application have their downsides - mainly that if you don't have internet connection or the server's down, then you don't have access to your data. The big upside is *sharing* and Airtable is great for that. You can [share the whole database](https://support.airtable.com/hc/en-us/articles/205752117-Creating-a-base-share-link-or-a-view-share-link) with other people at church, or just [share a particular view](https://support.airtable.com/hc/en-us/articles/205752117-Creating-a-base-share-link-or-a-view-share-link)
* Airtable has an API - so if you want to go crazy, you could [build an Alexa skill](https://aussieskills.netlify.app/gympie-presbyterian) so you can ask Alexa what you're preaching on next week 😳.
* [Importing data from spreadsheets](https://support.airtable.com/hc/en-us/articles/360057784594) - this could be used if you've got existing Excel or Google Sheets data which you want to incorporate into Airtable.

# Want to find out more?
Here's a great 4 minute introduction from the Airtable team.

{{<youtube pRUB4nnUp9o >}}

# What about...
There is another popular application in similar space to Airtable, which I thought could be even more powerful—-[notion](//notion.so). I had a look at it recently, and although it could be used for sermon preparation (because everything is a 'page'...you could even write the sermon in the app) the database feature isn't as complete. ~~In particular, it lacks a way to [count the links between tables](https://support.airtable.com/hc/en-us/articles/360042807213-Count-field-overview). I use the counting feature in my database to quickly see the number of weeks in each sermon series.~~ (EDIT: I've discovered ['Rollup' Fields](https://www.notion.so/Relations-rollups-fd56bfc6a3f0471a9f0cc3110ff19a79#cbc0bff512d441f4a78f6a8dd0770c99) in Notion, and just started testing replacing Airtable with Notion. Watch this space!)
