---
title: "Agenda"
date: 2022-04-08T08:20:56+10:00
tags: ["mac", "ios", "review"]
categories: ["tech"]
---
[Agenda](https://agenda.com) is one of the many (many!) applications that have come into the plaintext notetaking sphere over the past few years. Agenda's main point of difference is its date based organisation - and integration with calendars (and reminders).

![Screen Shot of Agenda Interface](/uploads/2022/04/AgendaScreenShot.png)

I started using Agenda when I realised I didn't have a 'place' to jot down notes and ideas in the lead up to meetings. As a minister, I'm involved with lots of formal meetings with elders, managers (board), and denominational committees. In the lead up to meetings, I often get ideas for things that could be put on the agenda or reflections on topics that will be discussed. These ideas don't require a full word processor document - it's normally just a dot-point or two. To my mind, they don't belong in my reminders system, and the 'notes' field in the calendar is too small and provides no formatting options. Agenda's text-based ([Markdown](https://www.markdownguide.org)) note taking provides a lightweight, simple interface for jotting down thoughts and ideas in the lead up to and during meetings. (You don't need to know Markdown to use the app, as traditional formatting keyboard shortcuts are supported.)

The real power of Agenda is its integration with calendar and Apple reminders. Every note can be associated with a date (which is just used for sorting notes) or *with calendar events*. When a note is linked to an event (which is done by clicking the calendar icon in the top right corner or typing `\assign-date` in Version 14 or newer), Agenda inserts a link into the note field in the Calendar entry - so you can quickly jump to the note from your calendar app.

Agenda is available throughout the Apple ecosystem—Mac, iPhone, and iPad. Data is synced through iCloud, and they take full advantage of new Apple technology like widgets.

A big improvement in version 14 (released in early April 2022) is that attached files can now be edited (at least on MacOS). Previously if you put attached a file (say, a financial report) to a note, it could be viewed using Quickview or a full application. If, however, you wanted to change something (for example, rotate pages in a PDF that included both portrait and landscape pages) this required exporting the file out of Agenda (e.g. dragging it to the desktop), deleting the file in Agenda, and then reimporting. Although that's not too difficult a workflow, I found it too fiddly, so I often didn't bother. Having files editable without export-import makes things much easier.

Agenda is **free** to use, with premium features costing about $50/year (Australian). Agenda has an interesting premium model, where purchasing premium gives you continued access to all premium features released in the following 12 months forever. That means if you pay for premium in 2022, but don't in 2023, you still get all the premium features released during 2022 or earlier. I think this is a great model, and well worth paying for - at least once!

Since it's free, just download it and give it a try. And I highly recommend their [YouTube channel](https://youtube.com/c/AgendaApp) for tutorials and ideas on how to make the most of Agenda.
