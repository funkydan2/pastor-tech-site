+++
categories = ["tech"]
date = "2018-06-09T21:01:37+10:00"
tags = ["howto", "printing", "MacOS"]
title = "Printing A5 (and smaller) on A4"
+++
How do you print A5 or A6 flyers when you've only got an A4 printer?

For a long time, I had a convoluted process involving PDF files and duplicating pages so that I could print two or more pages on a single A4 sheet, ready for the guillotine.

<div style="width:100%;height:0;padding-bottom:75%;position:relative;"><iframe src="https://giphy.com/embed/gIqusaeYxgSiY" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/world-robespierre-zumbachs-gIqusaeYxgSiY">via GIPHY</a></p>

I've just discovered a little trick in MacOS _Preview_ that does this in one (or two) clicks.

In the expanded print settings dialogue, there's an option called _'Copies per page:'_ which does as advertises—prints multiple copies of a document on a single page.

![](/uploads/2018/06/Print_Settings_Dialog.png)

And it doesn't just work with single page documents. I've tested it with a two page/double sided flyer, and it worked like a charm. All I needed to do was set _'Copies per page: '_ to **2** and then under _Layout_ set _'Two-sided: '_ to _Short-Edge binding_ and away I went. If I set the number of copies to 5, I would end up with 10 A5 copies.

Easy!
