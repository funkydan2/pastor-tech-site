---
title: Publish Sermon (Part 6)
date: 2018-04-06 02:31:30 +0000
tags:
- howto
- web
- wordpress
- b2
- sermon manager
categories: []

---
This howto assumes you already have

1. \[Sermon Manager installed and working on your Wordpress website\]({{< relref "setup-wordpress-for-podcasts.md" >}})
2. \[An account with Backblaze B2\]({{< relref "setup-b2.md" >}}) or another cloud host where you can store the MP3 files of the sermon recording.
3. \[Recorded\]({{< relref "record-sermon.md" >}}) a sermon, edited it, and \[created a MP3 file\]({{< relref "create-mp3.md" >}}).

Now it's time to publish the sermon.

# Create a New Sermon in Sermon Manager

First, log in to your Wordpress dashboard and select _+New->Sermon_.

![New Sermon](/uploads/2018/04/SermonPodcastHowto/SermonManager-NewSermon.png#float-right)

This brings up a screen where you can fill in details about the sermon, like its title, the date it was preached, Bible passages etc. Fill in all the details as they'll be used to display the sermon.

Lower down on the main screen there's a section titled **Sermon Files**. This is where you put in the details of your MP3 file.

![MP3 Details](/uploads/2018/04/SermonPodcastHowto/SermonManager-MP3Details.png)

* In _Location of MP3_, paste in the _Friendly URL_ which you copied from Backblaze earlier.
* You can get the _MP3 Duration_ from Auphonic on the same page where you download the processed MP3. It's not necessary to enter the duration, though it can make things work better in some podcast players.

(If you want more details on how to do this, [have a look at this tutorial from ARMD](https://andrewrminion.com/2017/05/backblaze-b2-sermon-manager-storage/).)

In the right-hand column, there's some extra data you can enter for your sermon.

![New Sermon](/uploads/2018/04/SermonPodcastHowto/SermonManager-MetaData.png#float-right)

You don't need to enter any of these details. However, they are used for displaying and filtering sermons. For the _Feature Image_ I create an image for each sermon series—something that can be used in Keynote/Powerpoint slides as well as other promotional things (website, Facebook, notices etc.). However, it's only cosmetic and not necessary. The other fields, such as _Preacher_, _Series_, and _Topics_ can be used by users of your website to find a particular sermon.

![New Sermon](/uploads/2018/04/SermonPodcastHowto/SermonManager-Filters.png)

Once you've entered all the information, click _Publish_ in the top right-hand box.

# Check Everything Has Worked

Then view your published post and make sure everything looks ok on your site.

If you're podcasting, it's worth checking the new sermon comes up in your favourite podcast player. Depending on how your podcast app works, and also whether you're using Feedburner, it can take between 15 and 30 minutes for new episodes to appear, so be patient. If the new recording doesn't appear, you should \[check your podcast feed in a feed validator\]({{< relref "setup-wordpress-for-podcasts.md#configure-podcast-feed" >}})

One of the common issues I've found with the podcast feed is having HTML code where there shouldn't be. Sermon Manager seems to be better at removing this, though if you're having problems, use the _Text_ input for the _Description_ and make sure there's no fancy looking HTML there.
![Sermon Description](/uploads/2018/04/SermonPodcastHowto/SermonManager-Description.png)

**Congratulations!** You've done it! Now you just need to remember \[why you did all this in the first place\]({{< relref "why-podcast-sermons.md" >}}):stuck_out_tongue_winking_eye:.