---
title: "Create MP3 (Part 5)"
date: 2018-04-05T21:28:35+10:00
tags: ["howto", "web"]
draft: false
---
Once we have our [exported WAV audio from Audacity]({{< relref "record-sermon.md" >}}), we need to create an MP3 file. This is because our WAV file is probably over 100 megabytes, whereas an MP3 file will weigh in closer to 6 Mb.

We're going to be using [Auphonic](https://auphonic.com) for this stage. Although Audacity can do almost all of these steps, it requires fiddling with lots of settings and having a great deal of technical audio knowledge. On the other hand, Auphonic does an amazing job of processing audio with a few clicks.

# 1. Create your sermon preset
Auphonic allows you to create presets. Since you're going to be uploading a sermon regularly and there are settings which you want to keep the same each time, it's worth setting up a preset.

To create a preset, once you've logged in, select *presets* from the top menu, and then click the red *New Preset* button.

In the preset settings, give your preset a name. Mine is *Sermon Podcast*. Then add the settings which will stay the same most of time. For me I have set:

1. A generic podcast image. It's the same image I use for the podcast RSS feed.
2. Some copyright information. The name of the church and it's web address.
3. The output files. You'll want to make an MP3 file at the lowest bitrate Auphonic provides (32 kbps) and select *mono*.
4. For *Audio Algorithms* I've kept everything at the default settings. This is the power of Auphonic and will make your record much easier to hear. Auphonic will increase the recording volume audio to the industry standard, so it's not a whole lot quieter than everything else people listen to. It will also remove any distracting hums and noises.
5. Save the preset

![Auphonic Presets](/uploads/2018/04/SermonPodcastHowto/Auphonic-Presets.png)

# 2. Process a single sermon
Now you've got a preset ready to go. It's time to process a sermon recording.

In Auphonic, click on *productions* and then *New Production*. Then from the drop-down menu at the top, select the preset you made in step one.

Now it's a matter of filling out the data for this recording.

1. Select the recording. This the [WAV file you recorded and edited in Audacity](/posts/record-sermon).
2. Fill out the Title, Artist (preacher), Album (sermon series), Subtitle (Bible passage), Summary (I put the sermon outline here, you could write the sermon's big idea), maybe some tags for searching.
3. Click *Start Production*.

Since Auphonic is web-based and your recording may be quite large (the WAV file may be more than 150 Mbs), it may take a fair bit of time for the recording to be uploaded to their servers. As the warning says, don't close the web browser tab while it uploads, but you can walk away from your computer, or use your computer for something else (I often start entering the sermon details into Sermon Manager while I wait).

Once the recording is uploaded and processed by Auphonic, you'll receive an email, and possibly a notification from your browser.

{{< figure src="/uploads/2018/04/SermonPodcastHowto/Auphonic-Results.png" alt="Auphonic Results" caption="This is a graphical display of what Auphonic does when it processes audio. Notice how the blue bar lines in the top graph are, on average, much higher, though the loudest peak is the same. This is showing how Auphonic increases the overall volume of a recording without introducing distortion." >}}

You can then download the processed MP3 and [upload it to Backblaze B2]({{< relref "setup-b2.md#how-to-upload-files" >}}) in preparation for posting to Wordpress.
