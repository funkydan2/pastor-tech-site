+++
title = "Email Updates"
date = 2021-08-10T13:53:44+10:00
images = []
tags = ["email"]
categories = ["tech"]
+++
Email newsletters are a great tool to communicate with the church family. Possibly for a small church, sending out an email with everyone's email in the BCC field might work—but often this method ends up with emails filtered out as spam. A great way around this is using a specific email 'marketing' system: one of the most popular ones being [mailchimp](https://mailchimp.com).

These tools are great, not only (generally) ensuring your email makes it to everyone at church, but they come with templates and other tools to make attractive and engaging newsletters.

In this post, I'm going to give a quick run-through of the workflow I use for writing email updates and also talk about some of the systems I've used.

# A Workflow for Email updates
## Write the draft in Drafts
One of the rules of thumb I mainly work with is to not enter anything directly into a web browser - at least not anything more than a sentence or two. Too often as you type a comment into a web page something goes wrong and you lose whatever you've done. (I've already mentioned this in a [take 5 review]({{< relref "take-5-ios#drafts" >}}).) I also find that writing directly into the web interface often ends up with confused or broken HTML.

So for email updates, I write the draft in [Drafts](https://getdrafts.com). Often I'll start the draft early in the week, noting down some of the things I'm going to include in the email update. Then when it's time to send out the email, I use the notes to write the full update.

I write using [markdown syntax](https://www.geeksforgeeks.org/introduction-to-markdown/) - so marking headings, emphasis, links, etc.

As I'm writing, if there's a place where I'd like to include a picture, I'll have a line saying something like `[[ Insert image/video of X ]]`.

[![Example of drafting an email in Drafts](/uploads/2021/08/drafts-email.png)](/uploads/2021/08/drafts-email.png)

Because most email marketing systems are organised using *blocks* (e.g. blocks for text, images, videos, headings), so as I'm writing the text, and thinking about images, I'll often split the draft into multiple entries using the [Split Draft](https://actions.getdrafts.com/a/16B) action.

## Copy HTML from Drafts to Email System
Once I'm pretty happy with the email, I copy an *HTML* version of what I've written using the [Copy as HTML](https://actions.getdrafts.com/a/1IM) action. This action produces clean, basic HTML code.

I then paste this HTML into the *View Source* section of a text block. (Normally two pointy brackets, `< >`, are used as the icon for the HTML editing mode.)

![Example of view source in Mailer Lite](/uploads/2021/08/mailer-lite-source.png)

## Finish Editing Email
After copying in the content and inserting images into the web interface, give your email a final read-through (if you're lucky, [Grammarly](//grammarly.com) will work with your preferred service and give hints to improve your writing) and then you're ready to send.

# A (brief) review of some email systems
## MailChimp
[MailChimp](https://mailchimp.com) is one of the most popular mass email systems, at least from what I can see with different churches and ministries. This is probably because it's easy to use, and has a generous [free tier](https://mailchimp.com/pricing/marketing/) (you can have up to 2000 people on your mailing list, and you can send up to 10 000 emails per month).

I used MailChimp for years but started having a problem with emails being blocked by some of the big ISPs used by older members at church.

## Zoho Campaigns
Since I use [Zoho for church email](https://mail.zoho.com/), I then gave [Zoho Campaigns](https://campaigns.zoho.com/campaigns/org657199498/home.do) a try. I didn't have any problems with emails being delivered with Zoho, which was great, everyone was receiving emails. However, Zoho Campaigns does something strange with its HTML templates (I think it uses inline CSS in each 'block', rather than a global CSS file) which meant that the nice, clean HTML brought across from Drafts didn't look consistent as with MailChimp (different parts of the email would end up with different font size or colour).

## MailerLite
At the moment I've settled on [MailerLite](https://www.mailerlite.com). It has loads of different kinds of 'blocks' to insert templates (videos converted to GIF, surveys, headings, columns). I find the 'lite' styling of its interface more appealing than MailChimp's. And it works well with the HTML created by Drafts.

Its [free tier](https://www.mailerlite.com/pricing) is not quite as generous as MailChimp, only allowing up to 1000 contacts, but you can send more emails—12 000 per month.

MailerLite seems to be more reliable in delivering to popular ISPs - I wonder if because it's a newer entry into the market it hasn't been abused by as many spammers. But for now, it's doing the trick for me.
