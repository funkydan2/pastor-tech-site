---
title: "Apple Pages Presenter Mode"
date: 2018-05-10T10:23:01+10:00
tags: ["iOS", "preaching", "apps", "howto", "review"]
draft: false
categories: ["tech"]
---

In a recent episode of [Mac Power Users](https://www.relay.fm/mpu/429) David and Katie briefly mentioned the new *Presenter Mode* in Apple Pages for iOS. They described it as a *teleprompter like* mode, where words scroll down the screen at a pace you set.

# A confession

Despite being a technophile, I don't preach *from* an iPad. I do use an iPad when preaching (more on that later) but not for the *text* of the sermon.

I've tried it once but didn't see any benefit over paper. I found the process of exporting to PDF, and flipping or scrolling pages within a PDF reader/editor to be unnecessarily complicated. One wrong tap and suddenly your annotating, or the words have shrunk. I've also found it better to have two pages of text visible at a time, which would be tiny on an iPad screen.

So, for many years I have taken into the 'pulpit':

1. Printed sermon manuscript. This is printed on A4 paper and placed into a plastic leaf display folder.

2. An iPad running Keynote for slides.[^1]

   [^1]: I run the sermon presentation from my iPad and send it to [Airserver](https://airserver.com). Airserver is an application which turns a PC into a virtual AppleTV. (Note: I should post about this setup one day.)

Yes, I'm a *full text* preacher (you can blame my year 8 English teacher, and [one of the authors of *Saving Eutychus*](http://www.savingeutychus.com) for this). The trick is learning to write the way you speak and to lay out the text with one sentence-fragment per line.[^2] I've been doing this for many years now, and since I tend to ramble when I'm speaking without notes (as you can see in the video below), I'm planning on sticking with it.[^3]
[^2]: *Saving Eutychus*, pp 54-55
[^3]: Occasionally people are surprised to find I'm preaching from a full-text (though I don't know how they could miss the page turns!) so I take it that it generally works.

# How it works

The best way to understand how this works is to see it in action. So here's a quick and dirty screen recording (with no preparation, so sorry for the ums, aahs, and pauses!) so you can see. Or jump into pages on an iOS device, click the *three dots* (top right-hand corner) and click *Presenter Mode* to have a go yourself!

{{< youtube mxxsBJ1kgvs >}}

The interface is not at all attempting to imitate the printed page, which is unusual for a [WYSIWYG](https://en.wikipedia.org/wiki/WYSIWYG) word processor. Instead of presenting the document with page breaks, it becomes a continuous stream of text. In _Presenter Mode_, lots of formatting is ignored (such as font size, style) but other details like font colour and emphasis (italic and bold) are displayed.

# What I needed to change (or, what Apple could improve)

Over some years I've developed a [Pages template](/uploads/2018/05/Sermon_Print.template) which I use for printed sermons. The template works well as it means I don't allow myself to waste time each week fiddling with fonts and settings. It

* uses a large sans-serif font for the body (14 points) which makes reading very easy, and helps me to keep my sentence fragments short.
* has a *quotation* style which is indented and has a smaller font. This helps Bible quotes to stand out in the manuscript. The smaller font also means I'm going to obviously and visibly *read* the Bible passages from my manuscript, which is a small hint to the congregation for them to read along with me in their Bible.
* has styles for prayers (smaller, italics) and various headings and footnotes.

This template has worked well for me. However, since *Presenter Mode* ignores most of these formatting choices (which is an interesting choice for a WYSIWYG word processor), I've had a first go of creating a [new template which makes greater use of colours and emphasis](/uploads/2018/05/Sermon_Teleprompt.template). In my first trial, it worked ok. Though I'd prefer if *Presenter Mode* preserved *relative* font sizes.

# Is there an alternative?

I'd like to be able to write my sermons in [Markdown](https://daringfireball.net/projects/markdown/), partly for future proofing (I expect plain text documents will be able to be opened by computing devices decades from now), and also for simplicity when writing. But I haven't gone towards this writing method previously because Markdown is designed for writing which is going to be published online, rather than writing which is going to be printed.

However, if I'm moving towards using an iPad for preaching, then it may work well to write in Markdown and then use a teleprompter display. [Brett Terpstra has developed a web-based application to do this](http://brettterpstra.com/share/promptdown/mobile.html). Although it can work in a browser on an iPad, the workflow of writing text, and then copying and pasting the markdown into a browser at the last minutes seems full of risks for preaching. What I'd love is a native iOS app which contains the full workflow from writing text to displaying in presenter mode, which from my search of the App Store doesn't currently exist.

Have you given *Presenter Mode* a go, or do you know of a Markdown-based application to do this? If so, [get in touch]({{< relref "/contact.md" >}}).
