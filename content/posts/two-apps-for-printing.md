+++
categories = ["tech"]
date = "2019-04-27T10:00:00+10:00"
tags = ["mac", "printing"]
title = "Two apps for printing"

+++
Here are two applications I regularly use to help with printing

# Booklet

At church, we often print booklets: for Bible studies, new members' morning teas, handouts for weddings or funerals. How can easily create a booklet?

In the past, I used [_Create Booklet_](https://www.thekeptpromise.com/CreateBooklet/) which was a free utility. It connected into the _Print_ dialogue—down where you can _Save to PDF_, if you installed this utility you could _Create Booklet_ from any application with one click. At one update of MacOS, the free release stopped working, and the developer released a new version, [which sells in the App Store for around AUD $30 ($19.99 US)](https://itunes.apple.com/us/app/create-booklet-2/id1350225911 "Create Booklet 2 in Mac App Store"). I'm sure it's a great utility, but too rich for my tastes.

To replace _Create Booklet_ I've discovered [_Booklets_](https://createbooklets.com). It has one downside (I'll come to that next) but is priced much more reasonably at $1.99 US (I think around AUD $2.95 ) [in the Mac App Store](https://itunes.apple.com/au/app/booklet/id1375737884?mt=12 "Booklets in the MAS"). Unlike _Create Booklet_, _Booklets_ runs as a standalone app. It opens a PDF file and creates a booklet, ready for printing, by clicking the 'Create Booklet' button—it's that easy. The only downside is since it doesn't work inside the print dialogue, if you want to make a booklet for, say, a Pages file, you need to first export to PDF before importing into _Booklets_. But for 1/10 of the price - that's a step I'm willing to make.

![Booklets Screenshot](/uploads/2019/04/Booklets.png)

# Mindcad Tiler

Creating booklets is a problem of printing (usually) four A4 pages onto one double-sided sheet, ready for folding and stapling, but what if you've got a normal, A4 printer, and want to make something bigger? This is where [Mindcad Tiler](http://mindcad.com/tiler.html) comes in.

This app takes a single PDF document and formats it so it can be blown up and printed over multiple pages to make a banner.

![Tiler Screenshot.](/uploads/2019/04/Tiler.png)

I don't find as much use for printing banners or posters as I do for creating booklets though I use this app a few times a year for creating visual aids for children's ministry.

[Mindcad Tiler](https://itunes.apple.com/us/app/mindcad-tiler/id432460453?mt=12) retails for about $4.99 US in the Mac App Store.
