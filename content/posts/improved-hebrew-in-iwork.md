---
title: "Improved Hebrew in iWork"
date: 2018-06-15T06:45:33+10:00
tags: ["hebrew", "iwork", "pages", "keynote"]
categories: ["tech"]
---
This morning Apple gave a little present to Biblical languages nerds with their updates to the iWork suite of apps (Pages, Keynote, Numbers) on both Mac and iOS.

Hidden right down the bottom of the release notes was this little gem

![Pages 7.1 Release Notes](/uploads/2018/06/Pages_7.1_RTL.png)

I haven't had a chance to look at what the improvements are, but as I'm currently preaching through Amos, I'll soon see!

You can read more about the release on [9to5 Mac](https://9to5mac.com/2018/06/14/pages-keynote-numbers-mac-ios/).
