---
title: "Sermon Podcast with Wordpress: Series Intro"
date: 2018-04-03T07:41:01+10:00
tags: ["howto", "web", "wordpress"]
draft: false
---

Many churches record sermons and share them online. This is a series of posts with all the information on how I record, edit, upload, and share sermons from your church.

1. [Why record sermons and share online?]({{< relref "why-podcast-sermons.md" >}})
2. [How to setup Wordpress for sermon podcasting.]({{< relref "setup-wordpress-for-podcasts.md" >}})
3. [How to setup Backblaze B2 for hosting files.]({{< relref "setup-b2.md" >}})
4. [How to record a sermon]({{< relref "record-sermon.md" >}}).
5. [How improve audio and create an MP3 file]({{< relref "create-mp3.md" >}}).
6. [How to upload and publish a sermon.]({{< relref "publish-sermon.md" >}})

This series assumes your church website is a self-hosted Wordpress based site. Some of this will be helpful if you use [wordpress.com](https://wordpress.com) to host your site, and other parts will be be useful if you use another [Content Management System](https://en.wikipedia.org/wiki/Content_management_system)/Website creator.

Although there's lots of steps in this howto, don't let that discourage you. Once you've done it a few times, you'll find it only takes 15 minutes to edit a recording and have it online...and you could do it while reading a book or even playing with your kids!
