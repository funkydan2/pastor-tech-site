---
title: "What a Notion"
date: 2021-04-25T15:00:38+10:00
lastmod: 2023-07-18T08:38:12+10:00
tags: ["Web"]
categories: ["tech"]
---
[Notion](//notion.so) is the hot new tool for personal productivity. It's often thought of as a note-taking tool, but with its support for relational databases, it can do much, much more.

Been trying Notion for almost two months. I thought I'd found a great tool for organising and preparing teaching at church. And although the application was beautiful to use, I've gone back to my tried and tested tools

So here's a brief review of [the good]({{< relref "#the-good" >}}), [the bad]({{< relref "#the-bad" >}}), and [the ugly (and, sadly, it's pretty ugly)]({{< relref "#the-ugly" >}}) of Notion for my use in ministry.

# Review

## The Good
I set up Notion to solve two problems.
1. To replace [Airtable]({{< ref airtable-for-planning.md >}}) as a preaching/teaching/everything church calendar.
2. To replace [OmniOutliner](https://www.omnigroup.com/omnioutliner) in my sermon and service preparation workflow.

I use OmniOutliner to take structured notes and create a detailed outline of a sermon _before_ I start writing the manuscript (I've found that outlining halves the time taken to write a full manuscript, and helps me to think about the whole sermon before I get stuck into the details). I also use OmniOutliner for writing our prayers and other things for church services. OmniOutliner is great because it's structured, and allows you to _focus_ on a particular part of your outline, or 'zoom out' to the whole. However, OmniOutliner locks you into a fairly structured way of writing for a whole document, so I've been interested in finding a replacement tool.

Notion came across as interesting because of the way it integrates databases with pages. I was able to pretty much import and replicate the functionality of my Airtable database (with five years worth of sermons and services) - though Notion doesn't have all the options for filtering views. Notion, however, is quite different from Airtable in that each *row* of a column is also a *page*. And using this paradigm I was able to do my sermon note-taking, outlining, and service planning, all in one document (page in Notion parlance).

I also created a bunch of other pages:
* a dashboard/bookmarks page which linked to all the tools I use for church
* a 'text bank' with prayers, creeds, bible verses for benedictions etc.

[![Notion database view](/uploads/2021/04/notion-database.png)](/uploads/2021/04/notion-database.png)
[![Notion dashboard/links page](/uploads/2021/04/notion-dashboard.png)](/uploads/2021/04/notion-dashboard.png)

I think this is a really powerful paradigm. I love having everything in one place, as I sometimes forget to check my Airtable base, and things slip through the cracks. Notion's writing tools are also really flexible, and I could choose how to write (paragraphs, lists, [toggle lists](https://www.notion.so/Writing-editing-basics-68c7c67047494fdb87d50185429df93e#1d62e265508c4bd9b0b056c459535eb5), images...).

## The Bad
However, not everything about Notion was good.

Notion is notoriously slow. *Nothing* is locally cached. Every time you open a new page, or even a new section of a page, it downloads the data from the servers. This means that scrolling down large databases is slow, clicking toggle lists to reveal new pages is slow. [Notion has recently made some big improvements to speed things up](https://www.notion.so/blog/faster-page-load-navigation).

Notion also doesn't have [the same flexibility as Airtables to filter](https://support.airtable.com/hc/en-us/articles/360003695134-Guide-to-filters-and-record-visibility) data in views. I find a view of the next six weeks of services seems to be the sweet spot for me. However, in Notion I can either filter by particular dates (so I can create a view for everything in a year or term) or 'the next week' or 'the next month' - but I can't create a filter for 'the next 42 days' like Airtable supports.

Finally, some formatting options are not quite the way I'd like for outlining - particularly not being able to add heading formatting to toggle lists. I'm used to the way OmniOutliner works, where everything is a toggle list, and you can apply formatting (either through a 'list level' template or manually for each row/block) to both full rows as well as individual words in a row. This meant my document didn't *quite* look as I wanted, but it was far from a deal-breaker.

## The Ugly
The deal-breaker came on Friday this week.

Having translated the preaching passage on Monday, on Wednesday I spent a few hours in deep exegetical work—asking questions of the text, looking up commentaries for answers and insights. I'd considered ways the passage could be illustrated and applied. I put together a detailed outline. Everything was ready for writing my sermon manuscript on Friday, and I had let it 'sit' for a day before coming back to it.

When I came back on Friday, ready to get into drafting the sermon, a feeling of dread came over me. I opened the page in Notion and the cupboard was bare! It had *nothing* I'd written on Wednesday; hours of work had evaporated into the clouds! Fortunately (or so I thought), Notion keeps a detailed [Page History](https://www.notion.so/Version-history-eec3af1f5bc64ba0a712ee5794e12a9c#7d8c7b91ce654cc4b78ec38272dde97a)...a minute-by-minute snapshot of every page. But when I looked at that, there was _no revisions_ listed from the whole day.

Notion has an 'in-app' support button, and I got straight on to that. During the day I conversed with the support team, but, sadly, they could not find any evidence of the notes or outlining I'd done on Wednesday.

The apparent reason for the lost work is that, since Notion is a web app despite everything appearing to be working fine, *none* of what I had written got stored 'in the cloud'. Due to being a web app, this could happen if (1) the Notion servers were offline or (2) my internet connection went offline. However, there were [no reported incidents](https://status.notion.so) during that week, and I was using lots of other online tools (email, searching the web, opening othe Notion pages) in the process of my research - so I can't see how I went offline.

This data loss is an absolute deal-breaker for me. No matter how nice it is to play with a new toy, and how convenient I found some of the tools, I don't have time to lose work - even rarely.

# Where to now?
For now, I've returned to my previous system of Airtable + OmniOutliner. I'll keep an eye on Notion, especially if it ever provides [some kind of offline/caching support](https://www.reddit.com/r/Notion/comments/a8zj56/100_offline_mode_not_coming/). I've also signed up for the Beta release queue for [AnyType](https://anytype.io) which is an Open Sources, offline first, Notion 'clone'—although it's [only at early stages of database support](https://community.anytype.io/d/206-february-update).
